const bodyParser = require('body-parser');
const cors = require('./cors.js');
const express = require('express');
const helmet = require('helmet');
const morgan = require('morgan');

const { catchAll, notFound } = require('./error');

const app = express();
const personRouter = require('./person/person.router');

app.use(helmet());
app.use(cors());
app.use(morgan('tiny'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.get('/', (req, res) => {
  res.json({ message: 'It works!' });
});

app.use('/api/persons', personRouter);

app.use(notFound);
app.use(catchAll);

module.exports = app;
