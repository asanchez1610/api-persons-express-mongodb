const mongoose = require('mongoose');

const PersonSchema = new mongoose.Schema({
  nombres: String,
  email: String,
  telefono: String
});

const Person = mongoose.model('Person', PersonSchema);

module.exports = Person;
