const Person = require('./person.model');

module.exports.check = async (req, res, next) => {
  const person = await Person.findById(req.params.id);
  if (!person) {
    throw Error('Person not found');
  }
  next();
};

module.exports.create = async (req, res) => {
  const person = new Person(req.body);
  await person.save();
  res.json(person);
};

module.exports.remove = async (req, res) => {
  await Person.findByIdAndRemove(req.params.id);
  res.json(req.params.id);
};

module.exports.list = async (req, res) => {
  const persons = await Person.find();
  res.json(persons);
};

module.exports.update = async (req, res) => {
  const data = await Person.findOneAndUpdate({ _id: req.params.id }, req.body, {
    new: true
  }).exec();

  res.json(data);
};

module.exports.view = async (req, res) => {
  const person = await Person.findById(req.params.id);
  res.json(person);
};
